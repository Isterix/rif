Vous avez besoin de Ruby et des Gems Nokogiri et HTTParty.

Pour chaque site avec des images, le programme crée un dossier.

Dans ce dossier se trouve toutes les images qui n'ont pas été ignorées par le programme.

Les images ignorées peuvent être des gifs de 1px sur 1px, des images que le site interdit de télécharger ou certaines images liées à Squarespace.
Si un dossier n'a aucune image, ça veut dire que le site ne contenait que ça.

Dans chaque dossier se trouve un fichier nommé "URL.txt" où se trouve l'URL du site ainsi que les erreurs rencontrées par le programme sur ce dernier.