require 'nokogiri'
require 'httparty'
require "open-uri"

$nom_domaines = [".com", ".net", ".fr", ".org", ".edu", ".gov", ".it", ".ch", ".uk", ".xyz", ".us"]

def url_generator
	$urls = []
	100.times do
		$url_name = "https://"
		url_length = rand(5..25)
		$website = (0...url_length).map { ('a'..'z').to_a[rand(26)] }.join
		$url_name << $website
		$url_name << $nom_domaines[rand(0..10)]
		$urls << $url_name
	end
	#$urls = ["https://vccf.org"] #Test a website out that way
	puts("\nUNE NOUVELLE LISTE A ETE CREE\n")
	image_finder
end

def image_finder
	for $url in $urls
		begin
			puts("\nScan de " + $url)
			unparsed_site = HTTParty.get($url)
			$parsed_site = Nokogiri::HTML(unparsed_site.body)
			$images = $parsed_site.css("img")
			if $images.to_s.include? "e"
				def welcoming_computer
					$image_numero = 0
					puts("Il y a des images à télécharger sur " + $url + " !")
					if File.basename(Dir.getwd) != "RiF" #The program may create folders within folders. This block is there to prevent most, if not all cases.
						Dir.chdir("../")
						puts("\nLe programme s'est déplacé. Il se trouve maintenant à: " + Dir.pwd)
					end
					if File.exists?($site_web_numero.to_s)
						puts("Un dossier au nom semi-aléatoire a été crée pour l'occasion.")
						dumb = "Dossier " + rand(1..10000).to_s
						folder = Dir.mkdir(dumb)
						Dir.chdir(dumb)
					else
						puts("Le dossier " + $site_web_numero.to_s + " a été crée pour l'occasion.")
						folder = Dir.mkdir($site_web_numero.to_s)
						Dir.chdir($site_web_numero.to_s)
					end
					$site_web_numero = $site_web_numero + 1
					test_redirection = HTTParty.get($url, follow_redirects: false)
					if test_redirection.code >= 300 && test_redirection.code < 400
						puts($url + " est probablement un lien de redirection !")
						if test_redirection.to_s.empty? == false
							if $parsed_site.to_s.count('e') > 400
								puts("En fait non.")
							else
								if test_redirection.to_s.include? "href="
									$url = test_redirection[test_redirection.index("href=") + 6..]
									$url = $url[0..$url.index('"') - 1]
								else
									puts("Ce n'est pas le cas, mais la page contient peu d'éléments...")
								end
							end
						else
							puts("Néanmoins, il ne sera pas suivi auquel cas il y aurait eu une erreur.")
						end
						puts("C'est à " + $url + " que le programme téléchargera les images.")
					end
					puts("Le programme s'est déplacé. Il se trouve maintenant à: " + Dir.pwd)
					fichier = File.new("URL.txt", "w")
					fichier.write("URL du site web: " + $url)
					for image in $images
						$image_numero = $image_numero + 1
						image = image.to_s
						if image.include? "base64"
							puts("-----\nCette image est très probablement encodée (en base64). Afin d'éviter toute erreur, elle sera ignorée par le programme. Désolé !")
							puts("Url non-nettoyée de l'image encodée: " + image + "\n-----")
						elsif image.include? "https://www.facebook.com/tr"
							puts("Une image 1px sur 1px est en fait un traqueur Facebook, elle sera donc passée.")
						elsif image.include? "googleads.g.doubleclick.net"
							puts("Une image 1px sur 1px est en fait un traqueur Google, elle sera donc passée.")
						else
							image = image[image.index("src=") + 5..]
							image = image[0..image.index('"') - 1]
							if image.include? "?"
								image = image[0..image.index("?") - 1]
							end
							if image[0..3] != "http"
								if image.include? (".com" || ".fr" || ".net" || ".org" || ".edu" || ".gov" || ".it" || ".ch" || ".uk" || ".xyz" || ".us")
									if image[0] = '/'
										image[0] = ''
										if image[0] = '/'
											image[0] = ''
										end
									end
									if image[0..4] != "http"
										image.insert(0, "https://")
									end
								else 
									image.insert(0, $url)
								end
							end
							if image.include? "{"
								if image.include? "}"
									puts("L'URL de l'image contient une variable optionelle qui sera supprimée. (" + image[image.index("{")..image.index("}")] + ")")
									if image[image.index("{") - 1] == "_"
										image[image.index("{") - 1] = ""
									end
									if image[image.index("}") + 1] == "x"
										image[image.index("}") + 1] = ""
									end
									image[image.index("{")..image.index("}")] = ""
								end
							end
							extension = image[-4..]
							if extension == "jpeg" || extension == "aspx" || extension == "webp"
								extension = image[-5..]
							end
							begin
								puts("Téléchargement de: " + image)
								open(image) {|f|
									File.open($image_numero.to_s + extension, "wb") do |file|
										file.puts f.read
										puts("Image téléchargée ! ^^")
									end
								}
							rescue => error
								puts("ERREUR: " + error.to_s)
								fichier.write("\n\nL'erreur suivante a eu lieu: " + error.to_s + "\nPour l'image: " + image)
								if image.include? "squarespace"
									puts("Si le programme échoue une fois à télécharger une image liée à Squarespace, il ne tente pas une seconde fois. \nL'image " + image + " sera passée, désolé !")
									fichier.write("\nL'image ci-dessus n'a pas été téléchargée car c'est une image liée à Squarespace.")
								elsif image.include? "class="
									puts("Ce n'était en fait pas une image, elle sera donc passée !")
									fichier.write("\nL'élément ci-dessus n'est en fait pas une image, il fût donc passé.")
								else
									begin
										image_fallback = image
										puts("Le programme va transformer l'URL pour voir si ça change quelque chose.")
										for domaine in $nom_domaines
											if image.include? domaine
												good_place = image.index(domaine) + domaine.length
												if image[good_place] != "/"
													image.insert(good_place, "/")
												else
													image[good_place] = ''
												end
											end
										end
										puts("Téléchargement de: " + image)
										begin
											open(image) {|f|
												File.open($image_numero.to_s + extension, "wb") do |file|
													file.puts f.read
													puts("Image téléchargée malgré l'erreur ! :D")
												end
											}
											fichier.write("\nL'erreur ci-dessus à été résolue grâce à cette image: " + image)
										rescue
											fichier.write("\nAinsi que pour l'image: " + image)
											image = image_fallback.sub($url, "")
											image.insert(0, "https:/")
											if image[7] != "/"
												image.insert(7, "/")
											elsif image[8] == "/"
												image[8] = ""
											end
											puts("Ca n'a pas marché. Téléchargement de: " + image + " à la place.")
											open(image) {|f|
												File.open($image_numero.to_s + extension, "wb") do |file|
													file.puts f.read
													puts("Image téléchargée malgré les erreurs ! Hourra !")
												end
											}
											fichier.write("\nL'erreur ci-dessus à été résolue grâce à cette image: " + image)
										end
									rescue
										puts("L'erreur n'a pas pu être résolue.")
										fichier.write("\nL'erreur ci-dessus n'a pas été résolue, l'image ne fût donc pas téléchargée.")
									end
								end
							end
						end
					end
					if File.basename(Dir.getwd) != "RiF"
						Dir.chdir("../")
						puts("Le programme s'est déplacé. Il se trouve maintenant à: " + Dir.pwd)
					end
					if fichier.closed?
						spaghetti_code = true
					else
						fichier.write("\n\nLe téléchargement des images sur cette page web est terminé !")
						fichier.close
					end
					puts("Toutes les images de la page principale du site web (" + $url + ") ont été téléchargées avec succès.")
				end
				welcoming_computer
			else
				puts("Aucune image sur " + $url)
			end
		rescue => e
			if defined?(fichier) == nil #Is this block even useful? Lol
				still_spaghetti_code = true
			else
				fichier.write("\nToutes les images n'ont pas pu être téléchargées à cause de l'erreur ci-dessous:\n " + e)
				fichier.close
			end
			puts(e)
			puts("Tâches finies sur " + $url)
		end
	end
end

$site_web_numero = 1

while "true" == "true"
	url_generator
end